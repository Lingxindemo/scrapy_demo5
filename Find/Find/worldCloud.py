
import jieba
from matplotlib import pyplot as plt
from wordcloud import WordCloud
from PIL import Image
import numpy as np

path = r'E:\python\scrapy_demo5\Find' #文件保存的地址
font = r'C:\Windows\Fonts\simkai.ttf'#字体的地址

text = (open(path + r'\Find\index.txt', 'r', encoding='utf-8')).read()
cut = jieba.cut(text)  # 分词
string = ' '.join(cut)
print(len(string))
# img = Image.open(path + r'\123.jpg')  # 打开图片
# img_array = np.array(img)  # 将图片装换为数组
stopword = ['xa0']  # 设置停止词，也就是你不想显示的词，这里这个词是我前期处理没处理好，你可以删掉他看看他的作用
wc = WordCloud(
    background_color='white',
    width=1400,
    height=1200,
    # mask=img_array,
    font_path=font,
    stopwords=stopword
)
wc.generate_from_text(string)  # 绘制图片
plt.imshow(wc)
plt.axis('off')
plt.figure()
plt.show()  # 显示图片
wc.to_file(path + r'\new.png')  # 保存图片
