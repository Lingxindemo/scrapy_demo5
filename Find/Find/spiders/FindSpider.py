# -*- coding: utf-8 -*-
import scrapy
import urllib.request
# from scrapy.http import FormRequest
from Find.items import FindItem
from bs4 import BeautifulSoup
import time

#知乎-会话：https://www.zhihu.com/question/54773510

class DbloginspiderSpider(scrapy.Spider):
    name = 'lxdemo'
    allowed_domains = ['douban.com']
    start_urls = ['https://accounts.douban.com/login']

    def parse(self, response):
        #首先获取验证图片地址并复制给 imgurl 变量
        imgurl = response.xpath('//img[@id="captcha_image"]/@src').extract()
        #由于验证码时有时无，因此需要判断如果有就手动输入
        if len(imgurl) > 0:
            print("有验证码返回...")
            #将验证图片保存到本地中
            local_path = r"lcy.png"
            urllib.request.urlretrieve(imgurl[0], filename=local_path)
            #定义接受验证码变量
            capt_value = input("请查看本地 lcy.png 图片并输入对应的验证码-> ")
            #设置带有验证码的 post 信息
            data = {
                "redir":"https://movie.douban.com/subject/1292052/comments?start=0&limit=20&sort=new_score&status=P",#要跳转的链接
                "form_email":"2629555648@qq.com",#用户名
                "form_password":"",#密码
                "captcha-solution":capt_value,#验证码
            }

        else:#此时不需要验证码
            print("无验证码登录...")
            #设置无验证码请求的参数
            data = {
                "redir":"https://movie.douban.com/subject/1292052/comments?start=0&limit=20&sort=new_score&status=P",#要跳转的链接
                "form_email":"2629555648@qq.com",#用户名
                "form_password":"",#密码
                }

        print("登录中......")
        #带参的登录请求
        return [scrapy.FormRequest.from_response(response,
                    #设置 cookie 信息   注：这两项在 settings.py 文件中设置即可
                    meta={"cookiejar":1}, #如果重写 start_requests()方法，那么该值必须对应请求里的 meta 中的键
                    #设置请求头模拟成浏览器
                    #headers=self.headers,
                    #设置 post 表单中的数据
                    formdata=data,
                    callback=self.mynode,
                    )]

    def mynode(self, response):
        print("登录成功......")
        for book in response.xpath('//div[@class ="mod-bd"]'):
            pinglun = book.xpath('div[@class="comment-item"]/div[@class="comment"]/p/span/text()').extract()
            next = book.xpath('div[@class="center"]/a[@class="next"]/@href').extract()
            if len(next)>0:
               for a in pinglun:
                  print(a)
                  s = open('index.txt','a+',encoding="utf-8")
                  s.writelines(a)
               for b in next:
                  print(b)
                  new_url = "https://movie.douban.com/subject/1292052/comments"+b
                  #设置爬取冷却时间，防止被ip封号
                  time.sleep(0.5)
                  yield scrapy.Request(new_url,callback=self.mynode,meta={"cookiejar":1})
            else:
                print("爬取完毕,地址无法再继续！！！")





